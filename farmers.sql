-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2016 at 06:58 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `madt`
--

-- --------------------------------------------------------

--
-- Table structure for table `farmers`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `farmers`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `phone`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Arnold', 'Masanso', '0793263301', 'arnold.masanso@gmail.com', '$2y$10$wkcOsDu/3lMd0ya5bY1cF.gUMbHNhDd6T8LvjJAEfdu5c0xme1LlW', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(2, 'Moses', 'Ayoma', '0793263301', 'ayoma.moses@gmail.com', '$2y$10$7yUsD0VHs/bTqijauI4gIufueGEiLkMP1psgfbZIXicRqrx96/6v.', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(3, 'Sandra', 'Iculet', '0793263301', 'sandra.iculet@gmail.com', '$2y$10$n7rA8A.2xecCY68/QUhxjOv67M3iTLFcfFExW0lP0/xhPAXebyIEm', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(4, 'Becky', 'Lakaleber', '0793263301', 'becky123@gmail.com ', '$2y$10$94a7AS.mp9VrbbXHYf7B1.RQ3RplKnPqPvWIt6He0sCkwE.XpWAOC', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(5, 'Arthur', 'Ssendagire', '0777889968', 'sendagire2@gmail.com', '$2y$10$LddxOQ8j0Suta37.0Nz8HePvx8rj42XgRZSiJ184tUkbpXkHcMUfC', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(6, 'Samuel', 'Kalyango', '0771488914', 'ksam@yahoo.com ', '$2y$10$x5Smu0ubWYwQ9.1VS0XutevOOjoGbTslBeCHRe3IdZoFXGie4mtkG', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(7, 'Daniel', 'Odong', '0778992204', 'odaniel@hotmail.com', '$2y$10$byEnlW1OKj1gu5.oUBVVa.dHYwtkSrqe44LEOVvR8NDGgTecpn.U6', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(8, 'Pamela', 'Abeja', '0783120203', 'ajpamela67@gmail.com', '$2y$10$YrW4Gg6o5AG76tjE/I90I.jsqS7LeqhO8CVJ80UzaXvH89NsVrhwq', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(9, 'Jonah', 'Mugoya', '0731200400', 'becky1234@gmail.com ', '$2y$10$/GmW7IQ5Yh8qdKYcUVGhAu3gZGwe19gX24TOEou3tVWh2SvguSv7S', '2016-04-21 22:23:17', '2016-04-21 22:23:17'),
(10, 'Patrick', 'Oryono', '0784275529', 'me@patrickoryono.co', '$2y$10$k54V1T23OWgM7L0XbijLz.pusZIAXgoqEIVVWRr4ZFkgCUXkMt4Iy', '2016-04-21 22:23:17', '2016-04-21 22:23:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
