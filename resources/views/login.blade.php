@extends('layouts.master')

@section('title'){{ "Login" }}@endsection

@section('content')

    <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h3>Login to MADT web portal</h3>
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @elseif(Session::has('message'))
                <div class="alert alert-warning">
                    {{ Session::get('message') }}
                </div>
            @endif
            <hr>
            <form action="" method="post">
                <input value="{{ csrf_token() }}" name="_token" type="hidden"/>
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <button class="=btn btn-success" type="submit">Login</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>

@endsection