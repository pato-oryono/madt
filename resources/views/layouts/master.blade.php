<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MADT| @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flat-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>
<body>
<div class="container-fluid">
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
</div>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}"></script>
</body>
</html>