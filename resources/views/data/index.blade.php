<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Data Entry</title>
</head>
<body>

<h3>
    @if (count($errors) > 0)
        <div style="color: red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @elseif (Session::has('success'))
        <div style="color: green">
            {{ Session::get('success') }}
        </div>
    @elseif(Session::has('error'))
        <div style="color: red">
            {{ Session::get('error') }}
        </div>
    @endif
</h3>

<form action="/data-entry" method="post">

    <div>
        <label for="produce_id">Produce</label>
        <select name="produce">
            @foreach($produces as $produce)

                <option value="{{ $produce->id }}">{{ $produce->id }}</option>
            @endforeach
        </select>
    </div>
    <br>
    <div>
        <label for="farmer">Farmer</label>
        <select name="farmer">
            @foreach($farmers as $farmer)

                <option value="{{ $farmer->id }}">{{ $farmer->id }}</option>
            @endforeach
        </select>

    </div>
    <br>

    <div>
        <label for="market">Market</label>
        <select name="market">
            @foreach($markets as $market)

                <option value="{{ $market->id }}">{{ $market->id }}</option>
            @endforeach
        </select>
    </div>
    <br>
    <div>
        <label for="price">Price</label>
        <input type="text" name="price" placeholder="price">
    </div>

    <br>

    <button type="submit">Submit</button>

</form>



</body>
</html>