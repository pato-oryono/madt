<?php

use Illuminate\Database\Seeder;

class MarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $markets = array(
            array(
                'name' => 'Gulu Market',
                'district'=> 'Gulu',

            ),

            array(
                'name' => 'Kitgum arket',
                'district'=> 'Kitgum',

            ),

            array(
                'name' => 'Lira Market',
                'district'=> 'Lira',

            ),

            array(
                'name' => 'Soroti Market',
                'district'=> 'Soroti',

            ),

            array(
                'name' => 'Mbale Market',
                'district'=> 'Mbale',

            ),

            array(
                'name' => 'Arua Market',
                'district'=> 'Arua',

            ),


        );

        foreach ($markets as $market){
            \App\Market::create($market);
        }
    }
}
