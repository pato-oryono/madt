<?php

use Illuminate\Database\Seeder;

class ProduceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produces = array(
            array(
                'name'=> 'Tomatoes',
                'category_id'=> 3,


            ),

            array(
                'name'=> 'Cabbages',
                'category_id'=>3,


            ),

            array(
                'name'=> 'Oranges',
                'category_id'=>3,


            ),

            array(
                'name'=> 'Carrots',
                'category_id'=>3,


            ),

            array(
                'name'=> 'Onions',
                'category_id'=>3,


            ),
            array(
                'name'=> 'Mangoes',
                'category_id'=>3,


            ),
            array(
                'name'=> 'Passion Fruits',
                'category_id'=>3,


            ),
            array(
                'name'=> 'Bananas',
                'category_id'=>3,


            ),

            array(
                'name'=> 'Sweet Potatoes',
                'category_id'=>4,


            ),

            array(
                'name'=> 'Irish Potatoes',
                'category_id'=>4,


            ),

            array(
                'name'=> 'Maize',
                'category_id'=>4,


            ),

            array(
                'name'=> 'Beans',
                'category_id'=>4,


            ),

            array(
                'name'=> 'Cassava',
                'category_id'=>4,


            ),

            array(
                'name'=> 'Vanilla',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Coffee',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Tea',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Tobacco',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Sisal',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Cocoa',
                'category_id'=>5,


            ),

            array(
                'name'=> 'Eggs',
                'category_id'=>1,


            ),

            array(
                'name'=> 'Kroulers',
                'category_id'=>1,


            ),

            array(
                'name'=> 'Broilers',
                'category_id'=>1,


            ),

            array(
                'name'=> 'Local chickens',
                'category_id'=>1,


            ),

            array(
                'name'=> 'Turkeys',
                'category_id'=>1,


            ),


            array(
                'name'=> 'Ducks',
                'category_id'=>1,


            ),


            array(
                'name'=> 'Goats(Mubende)',
                'category_id'=>2,


            ),


            array(
                'name'=> 'Goats(Saneen)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Goats(Small East African)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Goats(Teso Goat)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Goats(Galla)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Goats(Anglonubian)',
                'category_id'=>2,


            ),


            array(
                'name'=> 'Pigs(Large White)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Pigs(Camborough)',
                'category_id'=>2,


            ),


            array(
                'name'=> 'Pigs(Berkshire)',
                'category_id'=>2,


            ),


            array(
                'name'=> 'Pigs(Landrace)',
                'category_id'=>2,


            ),


            array(
                'name'=> 'Pigs(Duroc)',
                'category_id'=>2,


            ),

            array(
                'name'=> 'Milk',
                'category_id'=>2,


            ),
        );

        foreach ($produces as $produce){
            \App\Produce::create($produce);
        }
    }
}
