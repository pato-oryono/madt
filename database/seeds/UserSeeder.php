<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'firstname' => 'Arnold',
                'lastname' => 'Masanso',
                'phone'=>'0793263301',
                'email' => 'arnold.masanso@gmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Moses',
                'lastname' => 'Ayoma',
                'phone'=>'0793263301',
                'email' => 'ayoma.moses@gmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Sandra',
                'lastname' => 'Iculet',
                'phone'=>'0793263301',
                'email' => 'sandra.iculet@gmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Becky',
                'lastname' => 'Lakaleber',
                'phone'=>'0793263301',
                'email' => 'becky123@gmail.com ',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Arthur',
                'lastname' => 'Ssendagire',
                'phone'=>'0777889968',
                'email' => 'sendagire2@gmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Samuel',
                'lastname' => 'Kalyango',
                'phone'=>'0771488914',
                'email' => 'ksam@yahoo.com ',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Daniel',
                'lastname' => 'Odong',
                'phone'=>'0778992204',
                'email' => 'odaniel@hotmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Pamela',
                'lastname' => 'Abeja',
                'phone'=>'0783120203',
                'email' => 'ajpamela67@gmail.com',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Jonah',
                'lastname' => 'Mugoya',
                'phone'=>'0731200400',
                'email' => 'becky123@gmail.com ',
                'password' =>Hash::make('farmer'),

            ),

            array(
                'firstname' => 'Patrick',
                'lastname' => 'Oryono',
                'phone'=>'0784275529',
                'email' => 'me@patrickoryono.co',
                'password' =>Hash::make('farmer'),

            ),





        );

        foreach($users as $user){
            \App\Farmer::create($user);
        }
    }
}
