<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array(
            array(
                'name' => 'Poultry',
                'description' => ''

            ),

            array(
                'name' => 'Livestock',
                'description' => ''

            ),

            array(
                'name' => 'Vegetables and fruits',
                'description' => ''
            ),

            array(
                'name' => 'Foods',
                'description' => ''

            ),

            array(
                'name' => 'Cash Crops',
                'description' => ''
            ),


        );

        foreach ($categories as $category){
            \App\Category::create($category);
        }

    }
}
