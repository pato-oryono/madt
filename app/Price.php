<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = [
        'produce_id',
        'farmer_id',
        'market_id',
        'price'
    ];

    public function farmer()
    {
        return $this->belongsTo(Farmer::class, 'farmer_id');
    }

    public function produce()
    {
        return $this->belongsTo(Produce::class, 'produce_id');
    }

    public function market()
    {
        return $this->belongsTo(Market::class, 'market_id');
    }
}
