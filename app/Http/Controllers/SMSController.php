<?php

namespace App\Http\Controllers;

use App\Produce;
use Illuminate\Http\Request;

use App\Http\Requests;

class SMSController extends Controller
{
    public function index(Request $request)
    {
        //\Log::alert($request->all());

        $produces = new Produce();

        $message = $produces->message($request->get('message'));

        return response()->json([
            'events' => [
                [
                    'event' => 'send',
                    'messages' => [
                        [
                            'to' => $request->get('from'),
                            'message'=>$message
                        ]
                    ]
                ]

            ]
        ]);
    }

}
