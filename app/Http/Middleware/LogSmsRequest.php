<?php

namespace App\Http\Middleware;

use Closure;

class LogSmsRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        return $next($request);

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $response
     * @return mixed
     */
    public function terminate($request, $response)
    {

        /*\Log::alert($request->all());*/
        \App\Request::create(
            [
                'version' => $request->get('version'),
                'phone_number' => $request->get('phone_number'),
                'log' =>   $request->get('log'),
                'network' => $request->get('network'),
                'settings_version' => $request->get('settings_version'),
                'now' => $request->get('now'),
                'battery' => $request->get('battery'),
                'power' => $request->get('power'),
                'action' => $request->get('action'),
            ]
        );

        return $response;
    }
}
