<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {
    return view('home');
});

Route::post('/sms','SMSController@index'
);

Route::get('/farmers',function(){
    $farmers = \App\Farmer::all();
    //dd($farmers);
    return response()->json($farmers);
});

Route::get('/markets',function(){
    $markets = \App\Market::all();
    return response()->json($markets);
});

Route::get('/produces',function(){
    $produces = \App\Produce::with(['category', 'prices', 'farmer'])->get();
    return response()->json($produces);
});

Route::get('/produces/{id}',function($id){
    $produces = \App\Produce::with(['category', 'prices.market','prices.farmer'])->find($id);
    return response()->json($produces);
});

Route::get('/categories',function(){
    $categories = \App\Category::all();
    return response()->json($categories);
});

Route::get('/categories/{id}',function($id){
    $categories = \App\Category::with(['produces'])->find($id);
    return response()->json($categories);
});

Route::get('/prices',function(){
    $prices = \App\Price::with(['farmer','produce', 'market'])->get();
    return response()->json($prices);
});


route::get('/data-entry', function(){
    $prices = \App\Price::all();
    $markets = \App\Market::all();
    $farmers = \App\Farmer::all();
    $produces = \App\Produce::all();
    return view('data.index', [
        'prices'=>$prices,
        'markets'=> $markets,
        'farmers'=>$farmers,
        'produces'=>$produces
    ]);
});

route::post('/data-entry', function(){
    $data = Input::all();

    $validity = Validator::make($data, array(
        'produce' => 'Required',
        'farmer' => 'Required',
        'market' => 'Required',
        'price' => 'required'
    ));

    if($validity->fails()){
        return Redirect::to('data-entry')->withErrors($validity);
    }else{
        \App\Price::create(array(
            'produce_id' => $data['produce'],
            'farmer_id' => $data['farmer'],
            'market_id'=>$data['market'],
            'price'=> $data['price']
        ));

        return Redirect::to('data-entry')->with('success', 'Success');
    }






});

Route::get('/test', function(){

    return response()->json(\App\Produce::message('Tomatoes'));

});

Route::get('auth/login', function(){
   return view('login');
});

Route::post('auth/login', function(){
    $credentials = [
        'email'     => Input::get('email'),
        'password'  => Input::get('password')
    ];

    //$remember = Input::get('remember');

    return Auth::attempt($credentials) ? redirect::intended('/welcome') : redirect::to('/auth/login')->with('error', 'Invalid username or password');

});

Route::get('welcome', function(){
    return view('welcome');
});




