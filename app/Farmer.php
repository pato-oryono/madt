<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    protected $fillable = [
        'firstname',
        'lastname',
        'phone',
        'email',
        'password'
    ];

    public function market()
    {
        return $this->belongsToMany('\App\Market');
    }
}
