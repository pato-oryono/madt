<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = [
        'version',
        'phone_number',
        'log',
        'network',
        'settings_version',
        'now',
        'battery',
        'power',
        'action'
    ];
}
