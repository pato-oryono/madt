<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Produce
 * @package App
 *
 */
class Produce extends Model
{
    protected $fillable = [
        'name',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function farmer()
    {
        return $this->belongsTo(Farmer::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class, 'produce_id');
    }

    public function message($request)
    {
        /** @var Produce $produces */
        $produce = self::with(['prices', 'category', 'prices.market'])->where('name', $request)->first();

        if ($produce) {
            $message = 'Prices of ' . $request;

            return $message . ' ' . $produce->prices->map(function(Price $p) {
                return $p->market()->getResults()->name . ": " . $p->price;
            })->reduce(function($message, $price) {
                return $message . "\n" . $price;
            }, '');
        }else{
            return $message='Prices of '.$request.' not yet available.';
        }
    }

}
